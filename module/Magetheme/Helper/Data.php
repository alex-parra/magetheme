<?php

/**
 * Class Ebuynow_Magetheme_Helper_Data
 */
class Ebuynow_Magetheme_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function allowLogin()
    {
        $loginsOff = (bool)Mage::getStoreConfig('ebnmage/settings/loginsoff');
        return !$loginsOff;
    }


    public function allowSales()
    {
        $salesOff = (bool)Mage::getStoreConfig('ebnmage/settings/salesoff');
        return !$salesOff;
    }


    public function showPrices()
    {
        $hidePrices = (bool)Mage::getStoreConfig('ebnmage/settings/hideprices');
        return !$hidePrices;
    }

    public function showSwitcher()
    {
        $hideSwitcher = Mage::getStoreConfigFlag('ebnmage/settings/hideswitcher');
        return !$hideSwitcher;
    }

}
