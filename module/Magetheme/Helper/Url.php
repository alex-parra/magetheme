<?php

/**
 * Class Ebuynow_Magetheme_Helper_Url
 */
class Ebuynow_Magetheme_Helper_Url extends Mage_Core_Helper_Abstract
{
    public function getRedirectUrl($product)
    {
        $url = trim( $product->getData('ebn_redirect_url') );
        if ($product->getData('ebn_redirect_enabled') && $url) {
            return $url;
        }
        return null;
    }

    public function isRedirectUrlExternal($product)
    {
        if (!$product->getData('ebn_redirect_enabled')) {
            return false;
        }

        $url = $product->getData('ebn_redirect_url');
        if (empty($url)) {
            return false;
        }
        $redirectUrl = parse_url($url);
        $currentHost = Mage::app()->getRequest()->getHttpHost();

        if ($redirectUrl['host'] != $currentHost) {
            return true;
        }

        return false;
    }
}
