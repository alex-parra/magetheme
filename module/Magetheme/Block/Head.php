<?php

class Ebuynow_Magetheme_Block_Head extends Mage_Page_Block_Html_Head {

  const EBNMAGE_CSSJS_CACHE_ID = 'ebnmage_assetshash'; // config.xml > global > cache > types
  const EBNMAGE_CSSJS_CACHE_TAG = 'EBNMAGE_CSSJS_HASH';
  const EBNMAGE_CSSJS_HASHES_CACHE_KEY = 'fileHashes';
  const EBNMAGE_CSSJS_HASHES_CACHE_TIME = 31536000; // 1 year: 31536000 seconds

  private $hashesMap = null;
  private $hashesMapNeedsSaving = false;

  // Only the files listed here will be merged if merging is enabled.
  // Limit these to files that are needed on all pages and that are unlikely to change.
  private $noHashFiles = array(
    'prototype/prototype.js',
    'lib/ccard.js',
    'prototype/validation.js',
    'scriptaculous/effects.js',
    'varien/js.js',
    'varien/form.js',
    'mage/translate.js',
    'mage/cookies.js',
  );


  protected function &_prepareStaticAndSkinElements($format, array $staticItems, array $skinItems, $mergeCallback = null) {

    $designPackage = Mage::getDesign();
    $baseJsUrl = Mage::getBaseUrl('js');
    $items = array();

    if ($mergeCallback && !is_callable($mergeCallback)) {
      $mergeCallback = null;
    }

    $toMerge = array();

    // get static files from the js folder, no need in lookups
    foreach ($staticItems as $params => $rows) {
      foreach ($rows as $name) {
        $filePath = Mage::getBaseDir() . DS . 'js' . DS . $name;
        $fileHashSuffix = $this->getFileHashSuffix($filePath, $name);
        $fileUrl = $baseJsUrl . $name;
        if( $fileHashSuffix === 'not-found' ) continue;
        if( empty($fileHashSuffix) ) {
          $toMerge[$params][] = ($mergeCallback) ? $filePath : $fileUrl;
        } else {
          $items[$params][] = $fileUrl.$fileHashSuffix;
        }
      }
    }

    // lookup each file basing on current theme configuration
    foreach ($skinItems as $params => $rows) {
      foreach ($rows as $name) {
        $filePath = $designPackage->getFilename($name, array('_type' => 'skin'));
        $fileHashSuffix = $this->getFileHashSuffix($filePath, $name);
        $fileUrl = $designPackage->getSkinUrl($name, array());
        if( $fileHashSuffix === 'not-found' ) continue;
        if( empty($fileHashSuffix) ) {
          $toMerge[$params][] = $filePath;
        } else {
          $items[$params][] = $fileUrl.$fileHashSuffix;
        }
      }
    }

    $this->saveHashesMap();

    $html = '';
    foreach ($toMerge as $params => $rows) {
      $mergedUrl = false;
      if( $mergeCallback ) {
        $mergedUrl = call_user_func($mergeCallback, $rows);
      }
      // render elements
      $params = trim($params);
      $params = $params ? ' ' . $params : '';
      if( $mergedUrl ) {
        $html .= sprintf($format, $mergedUrl, $params);
      } else {
        foreach ($rows as $src) {
          $html .= sprintf($format, $src, $params);
        }
      }
    }

    foreach ($items as $params => $rows) {
      $params = trim($params);
      $params = $params ? ' ' . $params : '';
      foreach ($rows as $src) {
        $html .= sprintf($format, $src, $params);
      }
    }

    return $html;
  }




  private function getHashesMap() {
    if( ! empty($this->hashesMap) ) {
      return $this->hashesMap;
    }

    $hashesMap = $this->cacheGet(self::EBNMAGE_CSSJS_HASHES_CACHE_KEY);
    if( empty($hashesMap) || ! is_array($hashesMap) ) {
      $hashesMap = array('time' => time());
    }
    $this->hashesMap = $hashesMap;
    return $this->hashesMap;
  }




  private function saveHashesMap() {
    if( ! $this->hashesMapNeedsSaving ) return;
    $this->cacheSet(self::EBNMAGE_CSSJS_HASHES_CACHE_KEY, $this->getHashesMap(), self::EBNMAGE_CSSJS_HASHES_CACHE_TIME);
  }




  private function getFileHashSuffix($filePath, $fileName) {
    if( in_array($fileName, $this->noHashFiles) ) {
      return '';
    }
    if( ! is_file($filePath) ) {
      return 'not-found';
    }

    $fileHash = $this->getFileHash($filePath);
    return '?v='.$fileHash;
  }




  private function getFileHash($filePath) {
    $hashesMap = $this->getHashesMap();
    if( array_key_exists($filePath, $hashesMap) ) {
      $fileHash = $hashesMap[$filePath];
    } else {
      $hashForce = Mage::getStoreConfig('ebnmage/settings/cachebust');
      $fileHash = hash_file('adler32', $filePath) . $hashForce;
      $this->setFileHash($filePath, $fileHash);
    }
    return $fileHash;
  }




  private function setFileHash($filePath, $fileHash) {
    $this->hashesMapNeedsSaving = true;
    $hashesMap = $this->getHashesMap();
    $hashesMap[$filePath] = $fileHash;
    $this->hashesMap = $hashesMap;
  }




  private function useCache() {
    return false; // TEMP. To fix. Issue: Cache refresh does not regenerate...
    return Mage::app()->useCache(self::EBNMAGE_CSSJS_CACHE_ID);
  }




  private function prefixCacheKey($key) {
    return 'ebnmage-cssjshashes-'.$key;
  }




  private function cacheSet($key, $val, $time = 300, $tag = null) {
    if( ! $this->useCache() ) {
      return $val;
    }

    $prefixedKey = $this->prefixCacheKey($key);
    if( empty($tag) ) $tag = self::EBNMAGE_CSSJS_CACHE_TAG;

    Mage::app()->getCache()->save(serialize($val), $prefixedKey, array($tag), $time);
    return $val;
  }



  // Returns false (bool) if not found or invalid
  private function cacheGet($key) {
    if( ! $this->useCache() ) {
      return false;
    }
    $prefixedKey = $this->prefixCacheKey($key);
    return unserialize(Mage::app()->getCache()->load($prefixedKey));
  }

}
