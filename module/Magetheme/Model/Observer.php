<?php

class Ebuynow_Magetheme_Model_Observer extends Varien_Event_Observer {


  public function __construct() {
    // empty
  }


  // If GET param 'embed' is 1, set the root template to empty.phtml to show the content only.
  public function hideHeader(Varien_Event_Observer $observer) {
    $controllerAction = Mage::app()->getFrontController()->getAction()->getFullActionName();
    $exclude = ['checkout_onepage_index'];

    if (in_array($controllerAction, $exclude)) {
      return;
    }

    $session = Mage::getSingleton('core/session');
    $request = $observer->getData('event')->getData('action')->getRequest();

    if ($request->getParam('embed') === '1') {
      $session->setIsEmbed(1);
    }

    // Allow retuning to normal mode
    if ($request->getParam('embed') === '0') {
      $session->unsIsEmbed();
    }

    $layoutUpdate = '
    <reference name="root">
    <action method="setTemplate"><template>page/empty.phtml</template></action>
    </reference>
  ';

    if ($session->getIsEmbed() === 1) {
      Mage::app()->getLayout()->getUpdate()->addUpdate($layoutUpdate);
    }
  } // hideHeader



  // If GET param 'test' is 1, fake a success page
  public function orderSuccessTest(Varien_Event_Observer $observer) {
    $controllerAction = Mage::app()->getFrontController()->getAction()->getFullActionName();
    $request = Mage::app()->getRequest();

    if ($controllerAction === 'checkout_onepage_success' && $request->getParam('test') === '1') {

      // To test the checkout success page, we load the lastest order info and set it into session.
      // This allows us to bypass Magento's checks and show a success page for the latest order placed.

      $checkoutSession = Mage::getSingleton('checkout/session');

      // Below is same as: SELECT entity_id as order_id, quote_id FROM sales_flat_order ORDER BY entity_id DESC LIMIT 1;
      $latestOrder = Mage::getModel('sales/order')->getCollection()
        ->addFieldToSelect(['entity_id', 'quote_id'])
        ->setOrder('created_at', 'DESC')
        ->setPageSize(1)
        ->setCurPage(1);

      // Get values as array and cast to object
      $lastestOrderData = (object)$latestOrder->getFirstItem()->getData();
      $orderId = intval($lastestOrderData->entity_id);
      $quoteId = intval($lastestOrderData->quote_id);

      // Set the checked session variables
      $checkoutSession->setLastSuccessQuoteId($quoteId);
      $checkoutSession->setLastQuoteId($quoteId);
      $checkoutSession->setLastOrderId($orderId);

    }
  } // testSuccess



  public function alternateLinks() {
    $headBlock = Mage::app()->getLayout()->getBlock('head');
    $prod = Mage::registry('current_product');
    $categ = Mage::registry('current_category');

    if($headBlock && ($prod || $categ)) {
      $links = array();
      $cachedLinks = Mage::app()->loadCache('magento_ebuynow_alternate_'.Mage::helper('core/url')->getCurrentUrl());
      if( !$cachedLinks ) {
        $stores = Mage::app()->getStores();
        $storesAvailable = array();
        $cacheTags = array();
        $currentUrl = $headBlock->getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true));
        $baseUrl = $headBlock->getUrl('');

        if( $currentUrl === $baseUrl ) {
          foreach($stores as $store) {
            $storesAvailable[$store->getId()] = $store->getBaseUrl();
          }
        } elseif( $prod ) {
          $cacheTags = $prod->getCacheTags();
          $prodStores = $prod->getStoreIds();
          foreach( $prodStores as $prodStoreId ) {
            $storeProd = Mage::getModel('catalog/product')->setStoreId($prodStoreId)->load($prod->getId());
            if( $storeProd->isVisibleInCatalog() && $storeProd->isVisibleInSiteVisibility() ) {
              $storesAvailable[$prodStoreId] = Mage::getUrl($storeProd->getUrlPath(), array('_store'=>$prodStoreId));
            }
          }

        } elseif( $categ ) {
          $cacheTags = $categ->getCacheTags();
          $categStores = $categ->getStoreIds();
          unset($categStores[0]);
          foreach($categStores as $categStoreId) {
            $storeCateg = Mage::getModel('catalog/category')->setStoreId($categStoreId)->load($categ->getId());
            if( $storeCateg->getIsActive() ) {
              $categUri = Mage::getSingleton('core/url_rewrite')->getResource()->getRequestPathByIdPath('category/'.$categ->getId(), $categStoreId);
              $storesAvailable[$categStoreId] = $stores[$categStoreId]->getBaseUrl() . $categUri;
            }
          }

        }

        foreach ($stores as $store) {
          if( !array_key_exists($store->getId(), $storesAvailable) || !$store->getIsActive() ) continue;

          $storeCode = str_replace('_', '-', ($store->getCode() == 'default') ? 'en-us' : $store->getCode());
          $links[$storeCode] = strtok($storesAvailable[$store->getId()], '?');
        }

        Mage::app()->saveCache(serialize($links), 'magento_ebuynow_alternate_'.Mage::helper('core/url')->getCurrentUrl(), $cacheTags, 3600);

      } else {
        $links = unserialize($cachedLinks);
      }

      foreach ($links as $storeCode => $url) {
        $headBlock->addLinkRel('alternate"' . ' hreflang="' . $storeCode, $url);
      }
    }
    return $this;
  }



} // class
