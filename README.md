
## MageTheme for Magento 1.9.x
### created by Alex Parra

This modman module contains a theme framework and a helper module.  
  
  
The theme is split into a few 'parts':  
- ```theme-default``` contains the main theme files. This serves as the base for all 'skins'  
- ```theme-blank``` is a demo skin, mostly white. Good to duplicate for a new skin  
  
  
The ```Magetheme``` module includes the following features:  
- Embeded mode that hides the header and footer. See ```Observer.php › hideHeader()```  
- Success Page testing. See ```Observer.php › orderSuccessTest()```  
- CSS/JS cache busting. See ```Block/Head.php```  
  
  
## Compile less to CSS
- Ensure npm is installed
- Install less: ```npm install less -g```
- Install less-clean: ```npm install -g less-plugin-clean-css```
- At the module root: ```npm run motomods-less``` to compile the motomods theme less to css
- See package.json for other themes
  
  
## Compile Theme JS
- Ensure npm is installed
- Install uglify: ```npm install uglify-js -g```
- At the module root: ```npm run motomods-js``` to compile the motomods theme js
- See package.json for other themes
- See ```mergejs.sh``` for merging routine
