#!/bin/bash

root=$PWD

file1path=$1
file1name="${file1path##*/}"
dir1="${file1path:0:${#file1path} - ${#file1name}}"

files2=""

if [ ! -z "$2" ]
  then
    file2path=$2
    file2name="${file2path##*/}"
    dir2="${file2path:0:${#file2path} - ${#file2name}}"
    cd $dir2
    files2=`cat $file2name | awk '{print dir2$1}' dir2="$dir2"`
fi

cd $root
cd $dir1
files1=`cat $file1name | awk '{print dir1$1}' dir1="$dir1"`

files="${files2} ${files1}"

cd $root;

echo "Merging & Minifying to ${dir1}merged.min.js"
for file in $files; do
  echo " > "$file;
done

uglifyjs $files -c -o "${dir1}merged.min.js"
echo "Done."; echo;
