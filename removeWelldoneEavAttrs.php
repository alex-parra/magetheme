<?php

include 'app/Mage.php';
Mage::app();
$setup = Mage::getResourceModel('catalog/setup', 'catalog_setup');


$setup->removeAttribute('catalog_category','wd_nav_simple');
$setup->removeAttribute('catalog_category','wd_nav_top');
$setup->removeAttribute('catalog_category','wd_nav_btm');
$setup->removeAttribute('catalog_category','wd_nav_right');
$setup->removeAttribute('catalog_category','wd_category_lable');
$setup->removeAttribute('catalog_category','wd_colour_style');

$setup->removeAttribute('catalog_product','wd_videobox');
$setup->removeAttribute('catalog_product','wd_customtabtitle');
$setup->removeAttribute('catalog_product','wd_customtab');
$setup->removeAttribute('catalog_product','wd_customhtml');

die('Remove Welldone EAV Attributes: DONE');