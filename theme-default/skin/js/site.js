jQuery.noConflict();

jQuery(function($){

  var $win = $('window');
  var $doc = $(document);
  var $html = $('html');
  var $body = $('body');

  setTimeout(function(){
    $html.toggleClass('no-js js-loaded');
  }, 0); // delay execution to js end of stack

  var touch = ('ontouchstart' in window);
  $html.addClass( touch ? 'touch' : 'no-touch' );

  FastClick.attach(document.body);

  // Lazy Load Images. https://github.com/dinbror/blazy
  var bLazy = new Blazy({offset: 300, loadInvisible: true});

  // Mobile Menu Toggle
  var offCanvasOpen = false;
  $doc.on('click', '.mobilemenu-toggle', function(){
    offCanvasOpen = ! offCanvasOpen;
    $.scrollLock(offCanvasOpen); // TRUE: lock scroll, FALSE: unlock
    $html.toggleClass('offCanvasOpen');
  });


  // MoveTo Api
  var appendTo = $('[data-appendto]');
  if( appendTo.length ) {
    appendTo.each(function(){
      var $this = $(this);
      var target = $this.data('appendto');
      var $target = $(target);
      if( $target.length ) {
        $target.append($this);
      }
    });
  }
  var prependTo = $('[data-prependto]');
  if( prependTo.length ) {
    prependTo.each(function(){
      var $this = $(this);
      var target = $this.data('prependto');
      var $target = $(target);
      if( $target.length ) {
        $target.prepend($this);
      }
    });
  }

  // Reveal Api
  var reveal = $('[data-reveal]');
  if( reveal.length ) {
    reveal.each(function(){
      var $this = $(this);
      var revealToggle = $('<div class="reveal-toggle"><span>'+ $this.data('reveal') +'</span></div>');
      var revealWrapper = $('<div class="reveal-content"></div>');
      revealWrapper.append($(this).html());
      $this.empty().append(revealToggle, revealWrapper);
    });
  }
  $doc.on('click', '.reveal-toggle', function(){
    $(this).closest('[data-reveal]').toggleClass('js-open');
  });



  // Modals (more or less bootstrap)
  $doc.on('click', '[data-toggle="modal"]', function(ev){
    var $this = $(this);
    var targetSelector = $this.data('target');
    $this.blur();
    ev.preventDefault();
    $body.addClass('modal-open');
    $('.modal.show').removeClass('show');
    $(targetSelector).toggleClass('show');
  });
  $doc.on('click', '.modal', function(ev){
    // if click was not in or inside .modal-content, close modal
    if( $(ev.target).closest('.modal-content').length === 0 ) {
      ev.preventDefault();
      $body.removeClass('modal-open');
      $('.modal.show').toggleClass('show');
    }
  });
  $doc.on('click', '[data-dismiss="modal"]', function(ev){
    this.blur();
    ev.preventDefault();
    $body.removeClass('modal-open');
    $('.modal.show').toggleClass('show');
  });



  // Top Menu Dropdowns
  $doc.on('click', function(ev){
    if( $(ev.target).closest('li.parent').length == 0 ) {
      $('.top-menu .js-on').removeClass('js-on');
    }
  });
  $doc.on('click', '.top-menu li.parent > a', function(ev){
    var li = $(this).closest('li');
    if( ! li.hasClass('js-on') ) {
      ev.preventDefault();
      li.trigger('hover');
    }
  });
  $('.top-menu li').hover(
    function(){
      var $this = $(this);
      $this.siblings().removeClass('js-on');
      if( $this.hasClass('parent') ) {
        $this.addClass('js-on').parents('.parent').addClass('js-on');
      }
    },
    function(){
    }
  );


  // Skip Links
  $doc.on('click', function(ev){
    if( $(ev.target).closest(".skip-content, .skip-link").length ) return;
    $('.skip-active').removeClass('skip-active');
  });
  $doc.on('click', '.skip-link', function(e){
    e.preventDefault();

    var self = $(this);
    // Use the data-target-element attribute, if it exists. Fall back to href.
    var target = self.attr('data-target-element') ? self.attr('data-target-element') : self.attr('href');
    var elem = $(target);
    if( ! target || ! target.length || ! elem.length ) {
      elem = self.parent().find('.skip-content');
    }

    $('.skip-active').not(self).not(elem).removeClass('skip-active');
    self.toggleClass('skip-active');
    elem.toggleClass('skip-active');
  });

  $('#header-cart').on('click', '.skip-link-close', function(e) {
    var parent = $(this).closest('.skip-content');
    var link = parent.siblings('.skip-link');
    parent.removeClass('skip-active');
    link.removeClass('skip-active');
    e.preventDefault();
  });

  $doc.on('click', '.storeSwitcher.toggle:not(.flags-loaded)', function(){
    var $this = $(this);
    $('.storeSwitcher.modal').find('[data-flag]').each(function(){
      var $flag = $(this);
      var flagUrl = $flag.data('flag');
      $flag.css('background-image', 'url('+ flagUrl +')');
      $('.storeSwitcher.toggle').addClass('flags-loaded');
    });
  });


  // Using setTimeout since Web-Kit and some other browsers call the resize function constantly upon window resizing.
  var resizeTimer;
  $win.resize(function (e) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function () {
      $win.trigger('delayed-resize', e);
    }, 250);
  });


  /* -------------------------------------------------- */
  /* !- Tweak Selects */
  $('select').each(function(){
    $(this).wrap('<div class="select-wrap"></div>');
  });


  // Mark newsletter elements
  var registrationNewletterOptin = $('.customer-account-create #is_subscribed');
  if( registrationNewletterOptin.length ) {
    registrationNewletterOptin.closest('li.control').addClass('newsletter');
  }
  var myAccountNewsletters = $('.customer-account-index .welcome-msg + .box-account .col2-set .col-2');
  if( myAccountNewsletters.length ) {
    myAccountNewsletters.addClass('newsletter');
  }


  // Product View Qty +/-
  var $qty = $('#qty');
  if( $qty.length ) {
    var $addToCart = $qty.closest('.add-to-cart');
    if( ! $addToCart.length ) return;

    $addToCart.addClass('plus-minus');

    var qtyMinus = $('<span class="qty-btn qty-minus"><i>&#8211;</i></span>');
    $qty.before(qtyMinus);

    var qtyPlus = $('<span class="qty-btn qty-plus"><i>&#43;</i></span>');
    $qty.after(qtyPlus);

    $addToCart.on('click', '.qty-btn', function(ev){
      var btn = $(this);
      var qty = parseInt($qty.val());
      if( btn.hasClass('qty-minus') && qty > 1 ) {
        $qty.val(qty-1);
        bundleUpdatePrice($qty);
      }
      if( btn.hasClass('qty-plus') ) {
        $qty.val(qty+1);
        bundleUpdatePrice($qty);
      }
    });

  }



  // Product Pitch Video
  var pitchVideo = $('.product-pitch [data-video]');
  if( pitchVideo.length ) {
    pitchVideo.each(function(){
      var $this = $(this);
      var wrap = $this.parent();
      $this.addClass('pitchvideo-play');
      wrap.addClass('pitchvideo-wrap').data('video', $this.data('video'));
    });
    $doc.on('click', '.pitchvideo-modal', function(ev){
      var $this = $(this);
      if( $(ev.target).is($this) ) {
        $this.remove();
      }
    });
    $doc.on('click', '.pitchvideo-wrap', function(){
      var videoUrl = $(this).data('video');
      var isYoutube = videoUrl.match(/(?:youtu|youtube|youtube-nocookie)(?:\.com|\.be)\/([\w\W]+)/i);
      var videoPopup = $('<div class="pitchvideo-modal"></div>');
      if( isYoutube ) {
        var params = '?autoplay=1&modestbranding=1&controls=2&rel=0&showinfo=0';
        videoPopup.append('<div class="pitchvideo-frame youtube"><iframe src="'+ videoUrl+params +'" frameborder="0"></iframe></div>');
      } else {
        videoPopup.append('<div class="pitchvideo-frame htmlvideo"><video controls playsinline autoplay src="'+ videoUrl +'"></video></div>');
      }
      $body.find('.pitchvideo-modal').remove();
      $body.append(videoPopup);
    });
  }


  // Category Page
  if( $body.hasClass('catalog-category-view') ) {

  }


  // Product Page
  if( $body.hasClass('catalog-product-view') ) {

    // Product Collateral Side Image
    var productImgs = $('.product-img-box .product-image-gallery img');
    if( productImgs.length ) {
      var featImg = ( productImgs.length >= 2 ) ? productImgs.eq(1) : productImgs.eq(0);
      var prodCollateral = $('.product-view .product-collateral');
      prodCollateral.addClass('sideImg');
      prodCollateral.find('.product-collateral-wrap').append('<div class="sideImg" style="background-image:url('+ featImg.data('url') +');"></div>');
    }

    // Product Page Image Slider
    var prodImgGallery = $('.product-img-box .product-image-gallery');

    // - Thumbs (thumbs first, then main slider to use un-expanded html)
    if( prodImgGallery.find('.img').length > 1 ) {
      var $thumbs = prodImgGallery.clone();
      $thumbs.addClass('product-thumbs').insertAfter('.product-img-box .product-image-gallery');
      $thumbs.find('.img').each(function(){
        var $this = $(this);
        var imgUrl = $this.find('img').attr('src');
        if( imgUrl == undefined ) {
          imgUrl = $this.find('img').data('lazy');
        }
        $this.html('');
        $this.prepend($('<div class="bg-img"></div>').css('background-image', 'url('+imgUrl+')'));
      });
      $thumbs.slick({
        dots: false, arrows:true, infinite: false, speed: 300, focusOnSelect: true,
        slidesToShow: 8, slidesToScroll: 1,
        asNavFor: '.product-img-box .product-image-gallery',
      });
    }

    // - Main Slider
    prodImgGallery.slick({
      infinite: true, dots: false, speed: 500, arrows:true, fade:false,
      slidesToShow: 1, slidesToScroll: 1,
      asNavFor: '.product-img-box .product-thumbs',
    });


    var pdpOptions = $('.pdp-container1');
    if( pdpOptions.length ) {
      pdpOptions.find('.price-box').wrap('<div class="price-info"></div>');
      var pdpDiscount = pdpOptions.next('.price-info').find('.discount');
      if( pdpDiscount.length ) {
        pdpOptions.find('.price-info').append(pdpDiscount);
      }
    }

  }




  /* -------------------------------------------------- */
  /* !- Cart Page Tweaks */
  if( $body.hasClass('checkout-cart-index') ) {
    var cartTotals = jQuery('.cart-totals-wrapper');
    var cartForms = jQuery('.cart-forms');

    if( cartTotals.length && cartForms.length ) {
      cartTotals.after(cartForms);

      var cartShipping = cartForms.find('.shipping');
      cartShipping.addClass('interactive');
      cartShipping.on('click', 'h2', function(){
        cartShipping.toggleClass('state-open');
      });
    }

    var cartContainer = $('.col-main');
    var headerCart = $('.page-header .header-minicart');
    var cartForm = $('#shopping-cart-table').closest('form');
    var itemQtySelector = '#shopping-cart-table .input-text.qty';
    $(document).on('focus', itemQtySelector, function(){ $(this).data('val', this.value); });
    $(document).on('keypress', itemQtySelector, function(e){ if(e.keyCode === 13) { e.preventDefault(); $(e.target).trigger('blur') } });
    $(document).on('blur', itemQtySelector, function(){ updateCartQuantities.call(this) });
  } // checkout-cart-index

  function updateCartQuantities() {
    var $this = $(this);
    var cartTableSelector = '#shopping-cart-table';

    if( $this.data('val') !== $this.val() ) {
      cartForm.find(cartTableSelector).addClass('updating');
      cartContainer.find('.btn-checkout').html('Updating totals...');
      $.ajax({
        type: cartForm.attr('method'),
        url: cartForm.attr('action'),
        data: cartForm.serialize(),
        cache: false,
        error: function(){ alert('Product quantity change NOT saved. Please try again.') },
      }).done(function(response){
        var updatedCart = $('.col-main', response);
        var noItems = $('.checkout-cart-empty', updatedCart);
        if( noItems.length ) {
          $('.col-main').html(noItems);
        } else {
          updatedCart.find(cartTableSelector + ' [data-src]').each(function(){
            var el = $(this);
            el.is('img') ? el.attr('src', el.data('src')) : el.css('background-image', 'url('+ el.data('src') +')');
            el.removeAttr('data-src').addClass('b-loaded');
          });
          cartContainer.find(cartTableSelector).replaceWith(updatedCart.find(cartTableSelector));
          cartContainer.find('.cart-totals').replaceWith(updatedCart.find('.cart-totals'));
          cartContainer.find('> .messages').remove();
          cartContainer.prepend(updatedCart.find('> .messages'));
        }
        headerCart.html( $('.page-header .header-minicart', response).html() );
      });
    }
  }





  /* !- Checkout Tweaks */
  if( $body.hasClass('onestepcheckout-index-index') ) {

    var autofillImg = $('#mc-geolocation');
    var autofillTxt = $('<a class="autofillTxt" href="javascript:;">Autofill with my location</a>');
    var autofillWrap = autofillImg.closest('.input-box').parent().addClass('autofillWrap');
    autofillWrap.find('> label').append(autofillTxt);
    autofillImg.hide();

    autofillWrap.on('click', '.autofillTxt', function(ev){
      ev.preventDefault();
      $(this).blur();
      autofillImg.trigger('click');
    });

    var checkoutBtnRow = $('#one-step-checkout-place-order').closest('.row-mg');
    if( checkoutBtnRow.find('#one-step-checkout-order-review-newsletter-wrapper').length == 0 ) {
      checkoutBtnRow.addClass('newsletter-off');
    }

    // Fix shipping-modal z-index
    var shippingModal = $('#shipping-modal');
    if( shippingModal.length ) {
      $body.append(shippingModal);
    }

  }


}); // document ready


// Bundle update price by pressing plus/minus
function bundleUpdatePrice($qty) {
  if( typeof bundle == "undefined" ) return;

  var basePrice = bundle.reloadPrice();
  var qty = Number($qty.val());
  qty = qty <= 0 ? 1 : qty;
  jQuery("span.price").each(function() {
    var currency = jQuery(this).html()[0];
    jQuery(this).html(currency + (basePrice * qty).toFixed(2));
  });
}



